﻿using DustinDesktopReporting.Data.Models;
using DustinDesktopReporting.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DustinDesktopReporting.Repository
{
    public class UserRepository: IUserRepository
    {
        private DustinReportingDbContext _objDustinReportingDbContext = new DustinReportingDbContext();
        public UserModel ValidateUser(string username, string pwd)
        {
            try
            {
                var value = _objDustinReportingDbContext.Database.SqlQuery<UserModel>("Select * from dbo.UserProfile where  EmailId={0} and Password={1} and RoleId=2", username, pwd).FirstOrDefault();
                if (value == null)
                    return new UserModel();
                else
                    return value;
            }
            catch(Exception ex)
            {
                return null;
            }
        }        
    }
}
