﻿using DustinDesktopReporting.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DustinDesktopReporting.IRepository
{
    public interface IUserRepository
    {
        UserModel ValidateUser(string username, string pwd);
        //bool IsUserActive(string userid);
    }
}
