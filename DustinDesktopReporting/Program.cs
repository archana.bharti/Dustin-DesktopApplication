﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DustinDesktopReporting
{
    static class Program
    {
        private static System.Windows.Forms.Timer timer1;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {           
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            string email = Properties.Settings.Default.Email;
            timer1 = new System.Windows.Forms.Timer();
            timer1.Tick += new EventHandler(timer1_Tick);
            timer1.Interval = 900000;//120000;//  //180000;//900000;//In miliseconds
            timer1.Start();
            Application.Run(new Reports.Login());
        }
        
        private static void timer1_Tick(object sender, EventArgs e)
        {
            Writer.Log("Sync Started");

            SyncDataAuto singleton = SyncDataAuto.InstanceCreation();           
            Thread t1 = new Thread(new ThreadStart(singleton.DoStuff));
            t1.IsBackground = true;
            t1.Start();

            //SyncDataAuto obj = new SyncDataAuto();
            //Thread t1 = new Thread(new ThreadStart(obj.DoStuff));
            //t1.IsBackground = true;
            //t1.Start();
        }
    }
}
