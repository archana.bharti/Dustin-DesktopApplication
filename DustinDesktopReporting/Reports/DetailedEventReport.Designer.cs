﻿namespace DustinDesktopReporting.Reports
{
    partial class DetailedEventReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnUpload = new System.Windows.Forms.Button();
            this.lblFilePath = new System.Windows.Forms.Label();
            this.lblFolderPath = new System.Windows.Forms.Label();
            this.txtDelimiter = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbFormats = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbColNameHeader = new System.Windows.Forms.CheckBox();
            this.openFileDialogCSVFilePath = new System.Windows.Forms.OpenFileDialog();
            this.fbdCSVFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.dGridCSVdata = new System.Windows.Forms.DataGrid();
            this.btnImport = new System.Windows.Forms.Button();
            this.txtCSVFilePath = new System.Windows.Forms.TextBox();
            this.btnOpenFldrBwsr = new System.Windows.Forms.Button();
            this.txtCSVFolderPath = new System.Windows.Forms.TextBox();
            this.gbMainUploadData = new System.Windows.Forms.GroupBox();
            this.btnOpenFileDlg = new System.Windows.Forms.Button();
            this.gbMain = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.dGridCSVdata)).BeginInit();
            this.gbMainUploadData.SuspendLayout();
            this.gbMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnUpload
            // 
            this.btnUpload.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUpload.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnUpload.Location = new System.Drawing.Point(112, 24);
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Size = new System.Drawing.Size(280, 23);
            this.btnUpload.TabIndex = 1;
            this.btnUpload.Text = "Save";
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // lblFilePath
            // 
            this.lblFilePath.Location = new System.Drawing.Point(32, 47);
            this.lblFilePath.Name = "lblFilePath";
            this.lblFilePath.Size = new System.Drawing.Size(72, 20);
            this.lblFilePath.TabIndex = 12;
            this.lblFilePath.Text = "File Path:";
            this.lblFilePath.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFolderPath
            // 
            this.lblFolderPath.Location = new System.Drawing.Point(32, 23);
            this.lblFolderPath.Name = "lblFolderPath";
            this.lblFolderPath.Size = new System.Drawing.Size(72, 20);
            this.lblFolderPath.TabIndex = 11;
            this.lblFolderPath.Text = "Folder Path:";
            this.lblFolderPath.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDelimiter
            // 
            this.txtDelimiter.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.txtDelimiter.Enabled = false;
            this.txtDelimiter.Location = new System.Drawing.Point(368, 120);
            this.txtDelimiter.MaxLength = 1;
            this.txtDelimiter.Name = "txtDelimiter";
            this.txtDelimiter.Size = new System.Drawing.Size(24, 20);
            this.txtDelimiter.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(304, 120);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 23);
            this.label2.TabIndex = 9;
            this.label2.Text = "Delimiter:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbFormats
            // 
            this.cmbFormats.BackColor = System.Drawing.SystemColors.Info;
            this.cmbFormats.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmbFormats.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFormats.Items.AddRange(new object[] {
            "CSV Delimited",
            "Tab Delimited",
            "Custom Delimited"});
            this.cmbFormats.Location = new System.Drawing.Point(168, 120);
            this.cmbFormats.Name = "cmbFormats";
            this.cmbFormats.Size = new System.Drawing.Size(121, 21);
            this.cmbFormats.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(112, 120);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 23);
            this.label1.TabIndex = 7;
            this.label1.Text = "Format:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbColNameHeader
            // 
            this.cbColNameHeader.Checked = true;
            this.cbColNameHeader.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbColNameHeader.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cbColNameHeader.Location = new System.Drawing.Point(112, 88);
            this.cbColNameHeader.Name = "cbColNameHeader";
            this.cbColNameHeader.Size = new System.Drawing.Size(240, 24);
            this.cbColNameHeader.TabIndex = 6;
            this.cbColNameHeader.Text = "Column Name Header";
            this.cbColNameHeader.CheckedChanged += new System.EventHandler(this.cbColNameHeader_CheckedChanged);
            // 
            // openFileDialogCSVFilePath
            // 
            this.openFileDialogCSVFilePath.Filter = "CSV Files (*.csv)|*.csv|DAT Files (*.dat)|*.dat";
            this.openFileDialogCSVFilePath.Title = "Select the CSV file for importing";
            // 
            // dGridCSVdata
            // 
            this.dGridCSVdata.AlternatingBackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.dGridCSVdata.CaptionText = "Imported CSV Data";
            this.dGridCSVdata.DataMember = "";
            this.dGridCSVdata.HeaderBackColor = System.Drawing.Color.Black;
            this.dGridCSVdata.HeaderForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.dGridCSVdata.Location = new System.Drawing.Point(8, 192);
            this.dGridCSVdata.Name = "dGridCSVdata";
            this.dGridCSVdata.ReadOnly = true;
            this.dGridCSVdata.Size = new System.Drawing.Size(488, 208);
            this.dGridCSVdata.TabIndex = 5;
            // 
            // btnImport
            // 
            this.btnImport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnImport.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnImport.Location = new System.Drawing.Point(112, 160);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(280, 23);
            this.btnImport.TabIndex = 4;
            this.btnImport.Text = "Import CSV Data";
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // txtCSVFilePath
            // 
            this.txtCSVFilePath.BackColor = System.Drawing.SystemColors.Info;
            this.txtCSVFilePath.Location = new System.Drawing.Point(112, 48);
            this.txtCSVFilePath.Name = "txtCSVFilePath";
            this.txtCSVFilePath.ReadOnly = true;
            this.txtCSVFilePath.Size = new System.Drawing.Size(240, 20);
            this.txtCSVFilePath.TabIndex = 2;
            this.txtCSVFilePath.Text = "D:\\Test\\Test.csv";
            // 
            // btnOpenFldrBwsr
            // 
            this.btnOpenFldrBwsr.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOpenFldrBwsr.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnOpenFldrBwsr.Location = new System.Drawing.Point(368, 24);
            this.btnOpenFldrBwsr.Name = "btnOpenFldrBwsr";
            this.btnOpenFldrBwsr.Size = new System.Drawing.Size(24, 23);
            this.btnOpenFldrBwsr.TabIndex = 1;
            this.btnOpenFldrBwsr.Click += new System.EventHandler(this.btnOpenFldrBwsr_Click);
            // 
            // txtCSVFolderPath
            // 
            this.txtCSVFolderPath.BackColor = System.Drawing.SystemColors.Info;
            this.txtCSVFolderPath.Location = new System.Drawing.Point(112, 24);
            this.txtCSVFolderPath.Name = "txtCSVFolderPath";
            this.txtCSVFolderPath.ReadOnly = true;
            this.txtCSVFolderPath.Size = new System.Drawing.Size(240, 20);
            this.txtCSVFolderPath.TabIndex = 1;
            this.txtCSVFolderPath.Text = "D:\\Test";
            // 
            // gbMainUploadData
            // 
            this.gbMainUploadData.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.gbMainUploadData.Controls.Add(this.btnUpload);
            this.gbMainUploadData.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.gbMainUploadData.Location = new System.Drawing.Point(18, 433);
            this.gbMainUploadData.Name = "gbMainUploadData";
            this.gbMainUploadData.Size = new System.Drawing.Size(504, 56);
            this.gbMainUploadData.TabIndex = 3;
            this.gbMainUploadData.TabStop = false;
            this.gbMainUploadData.Text = "Save Data in Table";
            // 
            // btnOpenFileDlg
            // 
            this.btnOpenFileDlg.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOpenFileDlg.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnOpenFileDlg.Location = new System.Drawing.Point(368, 47);
            this.btnOpenFileDlg.Name = "btnOpenFileDlg";
            this.btnOpenFileDlg.Size = new System.Drawing.Size(24, 23);
            this.btnOpenFileDlg.TabIndex = 3;
            this.btnOpenFileDlg.Click += new System.EventHandler(this.btnOpenFileDlg_Click);
            // 
            // gbMain
            // 
            this.gbMain.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.gbMain.Controls.Add(this.lblFilePath);
            this.gbMain.Controls.Add(this.lblFolderPath);
            this.gbMain.Controls.Add(this.txtDelimiter);
            this.gbMain.Controls.Add(this.label2);
            this.gbMain.Controls.Add(this.cmbFormats);
            this.gbMain.Controls.Add(this.label1);
            this.gbMain.Controls.Add(this.cbColNameHeader);
            this.gbMain.Controls.Add(this.dGridCSVdata);
            this.gbMain.Controls.Add(this.btnImport);
            this.gbMain.Controls.Add(this.btnOpenFileDlg);
            this.gbMain.Controls.Add(this.txtCSVFilePath);
            this.gbMain.Controls.Add(this.btnOpenFldrBwsr);
            this.gbMain.Controls.Add(this.txtCSVFolderPath);
            this.gbMain.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.gbMain.Location = new System.Drawing.Point(18, 9);
            this.gbMain.Name = "gbMain";
            this.gbMain.Size = new System.Drawing.Size(504, 416);
            this.gbMain.TabIndex = 2;
            this.gbMain.TabStop = false;
            this.gbMain.Text = "Import CSV Data";
            // 
            // DetailedEventReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(540, 498);
            this.Controls.Add(this.gbMainUploadData);
            this.Controls.Add(this.gbMain);
            this.Name = "DetailedEventReport";
            this.Text = "CSV Reader";
            ((System.ComponentModel.ISupportInitialize)(this.dGridCSVdata)).EndInit();
            this.gbMainUploadData.ResumeLayout(false);
            this.gbMain.ResumeLayout(false);
            this.gbMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnUpload;
        private System.Windows.Forms.Label lblFilePath;
        private System.Windows.Forms.Label lblFolderPath;
        private System.Windows.Forms.TextBox txtDelimiter;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbFormats;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox cbColNameHeader;
        private System.Windows.Forms.OpenFileDialog openFileDialogCSVFilePath;
        private System.Windows.Forms.FolderBrowserDialog fbdCSVFolder;
        private System.Windows.Forms.DataGrid dGridCSVdata;
        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.TextBox txtCSVFilePath;
        private System.Windows.Forms.Button btnOpenFldrBwsr;
        private System.Windows.Forms.TextBox txtCSVFolderPath;
        private System.Windows.Forms.GroupBox gbMainUploadData;
        private System.Windows.Forms.Button btnOpenFileDlg;
        private System.Windows.Forms.GroupBox gbMain;
    }
}