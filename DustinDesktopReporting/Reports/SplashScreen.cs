﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DustinDesktopReporting.Reports
{
    public partial class SplashScreen : Form
    {
        public SplashScreen()
        {
            InitializeComponent();
        }

        //private void SplashScreen_Load(object sender, EventArgs e)
        //{
        //    Login ds = new Login();
        //    ds.WindowState = FormWindowState.Maximized;
        //    ds.Show();
        //    this.Hide();
        //}

        private void timer1_Tick(object sender, EventArgs e)
        {
            progressBar1.Increment(1);
            if (progressBar1.Value == 100) timer1.Stop();
        }
    }
}
