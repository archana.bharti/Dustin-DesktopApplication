﻿using DustinDesktopReporting.Data.Models;
using DustinDesktopReporting.IRepository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DustinDesktopReporting.Reports
{
    public partial class Profile : Form
    {
        private DustinReportingDbContext _objDustinReportingDbContext = new DustinReportingDbContext();
        IUserRepository _userRepo;
        bool IsOpen = true;

        public Profile()
        {
            InitializeComponent();
            GetProfileDetails();        
        }

        public void GetProfileDetails()
        {
            try
            {
                UserModel model = new UserModel();
                //UserModel model = new UserModel();
                //Guid? id = Login.EmployeeId;
                string email = Login.EmailId;
                 model = _objDustinReportingDbContext.Database.SqlQuery<UserModel>("Select FirstName, LastName ,EmailId,EmployeeId from dbo.UserProfile where EmailId =@Email", new SqlParameter("Email", email)).FirstOrDefault();
                ///textBox1 o = new textBox1()
                txtName.Text = model.FirstName.ToString();

                txtEmailId.Text = model.EmailId.ToString();
                txtEmpId.Text = model.EmployeeId.ToString();
            }
            catch(Exception ex)
            {

            }           
        }

        private void btnMenu_Click(object sender, EventArgs e)
        {
            if (IsOpen)
            {
                pnlMenu.Visible = true;
                IsOpen = false;
            }
            else
            {
                IsOpen = true;
                pnlMenu.Visible = false;
            }
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            Home ds = new Home();
            ds.WindowState = FormWindowState.Maximized;
            ds.Show();
            this.Hide();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Profile ds = new Profile();
            ds.WindowState = FormWindowState.Maximized;
            ds.Show();
            this.Hide();
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            Login.EmployeeId = null;
            Login login = new Login();
            login.Show();
            this.Hide();


            //MessageBox.Show("Application Going to Close");
            //Application.Exit();
           
        }
              
    }
}
