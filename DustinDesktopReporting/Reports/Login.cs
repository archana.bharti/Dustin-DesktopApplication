﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using DustinDesktopReporting.Data.Models;
using DustinDesktopReporting.Repository;
using DustinDesktopReporting.IRepository;

namespace DustinDesktopReporting.Reports
{
    public partial class Login : Form
    {
       
        private DustinReportingDbContext _objDustinReportingDbContext = new DustinReportingDbContext();
        IUserRepository _userRepo;
        public static Guid? EmployeeId;
        public static string EmailId;
        public Login()
        {
            Thread t = new Thread(new ThreadStart(SplashScreen));
            t.Start();
            Thread.Sleep(5000);
            InitializeComponent();
            Init_Data();
            t.Abort();          
            txtloginname.Text = "Email";
            txtpassword.Text = "Password";
            _userRepo = new UserRepository();
            Properties.Settings.Default.Email = "EmailId";
            Properties.Settings.Default.Password = "Password";
        }

        public void SplashScreen()
        {
            Application.Run(new SplashScreen());
        }

        private void lnkForgetpassword_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ForgotPassword ds = new ForgotPassword();
            ds.WindowState = FormWindowState.Maximized;
            ds.Show();
            this.Hide();
        }

        private void Init_Data()
        {
            if (Properties.Settings.Default.Email != string.Empty)
            {
                if (Properties.Settings.Default.Remember == "Yes")
                {
                    txtloginname.Text = Properties.Settings.Default.Email;
                    txtpassword.Text = Properties.Settings.Default.Password;
                    checkRemember.Checked = true;
                }
                else
                {
                    txtloginname.Text = Properties.Settings.Default.Email;
                }
            }
        }

        private async void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                if (checkRemember.Checked)
                {
                    Properties.Settings.Default.Email = txtloginname.Text;
                    Properties.Settings.Default.Password = txtpassword.Text;
                    Properties.Settings.Default.Remember = "Yes";
                    Properties.Settings.Default.Save();
                }
                else
                {
                    Properties.Settings.Default.Email = txtloginname.Text;
                    Properties.Settings.Default.Password = "";
                    Properties.Settings.Default.Remember = "No";
                    Properties.Settings.Default.Save();
                }

                //MessageBox.Show("1");
                var users = _userRepo.ValidateUser(txtloginname.Text, txtpassword.Text);
                if (users.EmailId != null)
                {                   
                    EmployeeId = users.EmployeeId;
                    EmailId = users.EmailId;
                    //UserModel model = new UserModel();
                    //txtloginname = users.EmailId;
                    Profile ds = new Profile();
                    ds.WindowState = FormWindowState.Maximized;
                    ds.Show();
                    this.Hide();

                    SyncDataAuto singleton = SyncDataAuto.InstanceCreation();
                    Thread t1 = new Thread(new ThreadStart(singleton.DoStuff));
                    t1.IsBackground = true;
                    t1.Start();

                    //SyncDataAuto obj = new SyncDataAuto();
                    //Thread t1 = new Thread(new ThreadStart(obj.DoStuff));
                    //t1.IsBackground = true;
                    //t1.Start();
                    //SyncDataAuto obj = new SyncDataAuto();
                    //await obj.DoStuff();
                }
                else
                {
                    // pictureBox4.Visible = false;
                    DialogResult result = MsgBox.Show("Username/Password does not exists!!", "", MsgBox.Buttons.OK, MsgBox.Icon.Error, MsgBox.AnimateStyle.FadeIn);
                    // MessageBox.Show("Username/Password does not exists!");
                    return;
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }


        private async void YourFunction()
        {
            // pictureBox4.Visible = false;
            await Task.Delay(2000);
            // pictureBox4.Visible = true;
        }

        private void txtloginname_Enter(object sender, EventArgs e)
        {
            if (txtloginname.Text == "Email")
            {
                txtloginname.Text = "";
            }
        }

        private void txtloginname_Leave(object sender, EventArgs e)
        {
            if (txtloginname.Text == "")
            {
                txtloginname.Text = "Email";
            }
        }

        private void txtpassword_Enter(object sender, EventArgs e)
        {
            if (txtpassword.Text == "Password")
            {
                txtpassword.Text = "";
            }
        }

        private void txtpassword_Leave(object sender, EventArgs e)
        {
            if (txtpassword.Text == "")
            {
                txtpassword.Text = "Password";
            }
        }
    }
}    
        
