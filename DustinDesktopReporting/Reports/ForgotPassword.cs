﻿using DustinDesktopReporting.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DustinDesktopReporting.Reports
{
    public partial class ForgotPassword : Form
    {
        private DustinReportingDbContext _objWebReportingDbContext = new DustinReportingDbContext();
       
        public ForgotPassword()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            
            Login page = new Login();
            page.WindowState = FormWindowState.Maximized;
            page.Show();
            this.Hide();
        
    }
        private void btnChangePassword_Click(object sender, EventArgs e)
        {
            try
            {

                ForgetPasswordModel obj = new ForgetPasswordModel();

                obj.EmailId = txtUsername.Text.ToString();
                obj.Password = txtpassword1.Text.ToString();

                if (obj.EmailId != null)
                {
                    int effected = _objWebReportingDbContext.Database.ExecuteSqlCommand("ForgetPassword @EmailId=@EmailId,@Password=@Password",
                                 new SqlParameter("EmailId", txtUsername.Text.ToString()),
                                 new SqlParameter("Password", txtpassword1.Text.ToString()));


                    DialogResult result = MsgBox.Show("Your Password is changed Successfully ", "", MsgBox.Buttons.OK, MsgBox.Icon.Info, MsgBox.AnimateStyle.FadeIn);
                    Login page = new Login();
                    page.WindowState = FormWindowState.Maximized;
                    page.Show();
                    this.Hide();
                    return;
                   
                }
                else
                {

                    DialogResult result = MsgBox.Show("Username does not exist..Please enter Valid UerName/Email!!", "", MsgBox.Buttons.OK, MsgBox.Icon.Error, MsgBox.AnimateStyle.FadeIn);
                    return;
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}
     
    
