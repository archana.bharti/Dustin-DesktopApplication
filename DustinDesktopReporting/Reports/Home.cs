﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DustinDesktopReporting.Reports
{
    public partial class Home : Form
    {
        private DustinReportingDbContext _objDustinReportingDbContext = new DustinReportingDbContext();
        bool IsOpen = true;
      
        public Home()
        {
            InitializeComponent();
        }

        private void btnMenu_Click(object sender, EventArgs e)
        {
            if(IsOpen)
            {
                pnlMenu.Visible = true;
                IsOpen = false;
            }
            else
            {
                IsOpen = true;
                pnlMenu.Visible = false;
            }
        }

        private void btnChangePath_Click(object sender, EventArgs e)
        {
            //OpenFileDialog ofd = new OpenFileDialog();
            //System.Windows.Forms.DialogResult dr = ofd.ShowDialog();
            //if (dr == DialogResult.OK)
            //{
            //    txtPath.Text = ofd.FileName;
            //}

            txtPath.Text = "";
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                txtPath.Text = folderBrowserDialog1.SelectedPath.Trim();
            }
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            Home ds = new Home();
            ds.WindowState = FormWindowState.Maximized;
            ds.Show();
            this.Hide();
        }

        private void btnProfile_Click(object sender, EventArgs e)
        {
            Profile ds = new Profile();
            ds.WindowState = FormWindowState.Maximized;
            ds.Show();
            this.Hide();
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            Login.EmployeeId = null;
            Login login = new Login();
            login.Show();
            this.Hide();

            //MessageBox.Show("Application Going to Close");
            //Application.Exit();
        }

        private void btnSavePath_Click(object sender, EventArgs e)
        {
            try
            {
                string email = Properties.Settings.Default.Email;
                txtPath.Text = txtPath.Text.Replace("'", "");
                var value = _objDustinReportingDbContext.Database.ExecuteSqlCommand("Update UserProfile set Path ='" + txtPath.Text + "' where EmailId ='" + email + "'");
                //update student.studentinfo set idStudentInfo = '" + this.IdTextBox.Text + "',Name = '" + this.NameTextBox.Text + "',Father_Name = '" + this.FnameTextBox.Text + "',Age = '" + this.AgeTextBox.Text + "',Semester = '" + this.SemesterTextBox.Text + "' where idStudentInfo = '" + this.IdTextBox.Text + "'; ";
                txtPath.Text = "";
            }
            catch(Exception ex)
            {
            }
        }
    }
}
