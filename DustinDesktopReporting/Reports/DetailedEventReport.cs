﻿using DustinDesktopReporting.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DustinDesktopReporting.Reports
{
    public partial class DetailedEventReport : System.Windows.Forms.Form
    {
        private DustinReportingDbContext _objDustinReportingDbContext = new DustinReportingDbContext();
        string strCSVFile = "";
        private bool bolColName = true;
        string strFormat = "CSVDelimited";
        //System.Data.Odbc.OdbcDataAdapter obj_oledb_da;
        string _secondColoumn;
        string _thirdColoumn;
        string _lastDownloadDate;
        DataTable dt = new DataTable(); 
        public DetailedEventReport()
        {
            this._objDustinReportingDbContext = new DustinReportingDbContext();
            InitializeComponent();
        }

        #region Form Load 
        private void frmMain_Load(object sender, System.EventArgs e)
        {
            try
            {
                cmbFormats.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            { }

        }
        #endregion


        #region Open Folder Browser Button 
        // On click of this button, the FOLDERBROWSERDIALOG opens where user can select the path of the folder 
        // containing .csv files
        private void btnOpenFldrBwsr_Click(object sender, EventArgs e)
        {
            try
            {
                if (fbdCSVFolder.ShowDialog() == DialogResult.OK)
                {
                    txtCSVFolderPath.Text = fbdCSVFolder.SelectedPath.Trim();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {

            }
        }

        #endregion


        #region Open File Dialog Button 

        // On click of this button, the openfiledialog opens where user can select .csv file 
        private void btnOpenFileDlg_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialogCSVFilePath.InitialDirectory = txtCSVFolderPath.Text.Trim();
                if (openFileDialogCSVFilePath.ShowDialog() == DialogResult.OK)
                {
                    txtCSVFilePath.Text = openFileDialogCSVFilePath.FileName.Trim();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            { }
        }
        #endregion


        #region Create schema.ini 
        /*Schema.ini File (Text File Driver)

		When the Text driver is used, the format of the text file is determined by using a
		schema information file. The schema information file, which is always named Schema.ini
		and always kept in the same directory as the text data source, provides the IISAM 
		with information about the general format of the file, the column name and data type
		information, and a number of other data characteristics*/

        private void writeSchema()
        {
            try
            {

                FileStream fsOutput = new FileStream(txtCSVFolderPath.Text + "\\schema.ini", FileMode.Create, FileAccess.Write);
                StreamWriter srOutput = new StreamWriter(fsOutput);
                string s1, s2, s3, s4, s5;
                s1 = "[" + strCSVFile + "]";
                s2 = "ColNameHeader=" + bolColName.ToString();
                s3 = "Format=" + strFormat;
                s4 = "MaxScanRows=25";
                s5 = "CharacterSet=OEM";
                srOutput.WriteLine(s1.ToString() + '\n' + s2.ToString() + '\n' + s3.ToString() + '\n' + s4.ToString() + '\n' + s5.ToString());
                srOutput.Close();
                fsOutput.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            { }
        }
        #endregion

        #region Function for deciding format of CSV File  

        // The .csv file format specifies, how the file is delimited, either by using 
        //comma,tab or custom delimited

        private void Format()
        {
            try
            {

                if (cmbFormats.SelectedIndex == 0)
                {
                    strFormat = "CSVDelimited";
                }
                else if (cmbFormats.SelectedIndex == 1)
                {
                    strFormat = "TabDelimited";
                }
                else
                {
                    strFormat = "Delimited(" + txtDelimiter.Text.Trim() + ")";
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            { }
        }
        #endregion


        #region Button Import CSV Data

        private void btnImport_Click(object sender, EventArgs e)
        {           
            //get path from database
            string value = _objDustinReportingDbContext.Database.SqlQuery<string>("select ReportPath from Path").FirstOrDefault();
            
            //Upload and save the file
            string csvPath = value + Path.GetFileName(txtCSVFilePath.Text);

            //DateTime creation = File.GetCreationTime("E:/ClientProjects/Dustin.Desktop/DustinDesktopReporting/DustinDesktopReporting/Resources/Uploads/Book2.csv");

            //("E:/ClientProjects/Dustin.Desktop/DustinDesktopReporting/DustinDesktopReporting/Resources/Uploads/") + Path.GetFileName(txtCSVFilePath.Text);
            //FileUpload1.SaveAs(csvPath);
            //Create a DataTable.


            dt.Columns.AddRange(new DataColumn[]
        {
                new DataColumn("SensorNodeId", typeof(string)),
                new DataColumn("DetectionLog", typeof(string)),
                new DataColumn("Datetime",typeof(string)),
                new DataColumn("IsTimeValid",typeof(string)),
                new DataColumn("SensorStatus",typeof(string)),
                new DataColumn("ApparelUniqueId",typeof(string)),
                new DataColumn("LastDownloadDate",typeof(string)),
        });
      

        //Read the contents of CSV file.
        string csvData = File.ReadAllText(csvPath);

          
            int k = 0;
            //Execute a loop over the rows.
            foreach (string row in csvData.Split('\n'))
            {
                if (!string.IsNullOrEmpty(row))
                {
                    if (k > 0)
                    {
                        dt.Rows.Add();
                    }
                    int i = 1;
                    k++;
                    //Execute a loop over the columns.
                    if (dt.Rows.Count > 0)
                    {
                       
                        

                        dt.Rows[dt.Rows.Count - 1][0] = _thirdColoumn;
                        dt.Rows[dt.Rows.Count - 1][6] = _lastDownloadDate;
                        foreach (string cell in row.Split(','))
                        {
                           if(i<6)
                            {
                                if(cell.Contains("System info code"))
                                {
                                    dt.Rows[dt.Rows.Count - 1][i] = 0;
                                }
                                else if(cell.Contains("Apparel entered field"))
                                {
                                    dt.Rows[dt.Rows.Count - 1][i] = 1;
                                }
                                else if(cell.Contains("Apparel exited field"))
                                {
                                    dt.Rows[dt.Rows.Count - 1][i] = 2;
                                }
                                else
                                {
                                    dt.Rows[dt.Rows.Count - 1][i] = cell;
                                }
                               
                                i++;
                            }
                           
                        }
                    }
                    else
                    {
                        int j = 0;
                        foreach (string cell in row.Split(','))
                        {
                           
                            switch(j)
                            {
                                case 1:
                                    _lastDownloadDate = cell;
                                    break;
                                case 2:
                                  
                                    _secondColoumn = cell;
                                    break;
                                case 3:
                                   
                                    _thirdColoumn = cell.Replace("-",":");

                                    break;
                               
                                default:
                                    break;

                            }
                           
                           
                            j++;
                        }
                    }
                }
            }

            //Bind the DataTable.
            dGridCSVdata.DataSource = dt;
           // dGridCSVdata.DataBind();
        }
        #endregion

        #region Button Insert Data 

        //Here we will insert the imported data in our database

        private void btnUpload_Click(object sender, EventArgs e)
        {
            try
            {

                // Create Dataset					
                DataSet da = new DataSet();
                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                {
                    // DateTime value =Convert.ToDateTime(dt.Rows[i][2]);
                    DateTime date = DateTime.ParseExact(dt.Rows[i][2].ToString(), "dd/MM/yyyy HH:mm:ss", null);
                    string s = date.ToString("MM/dd/yyyy HH:mm:ss");
                    DateTime maindate = Convert.ToDateTime(s);
                    // String  datetime= value.ToString("MM/dd/yyyy hh:mm ss tt");

                    DateTime lastdate = DateTime.ParseExact(dt.Rows[i][6].ToString(), "dd/MM/yyyy HH:mm:ss", null);
                    string st = lastdate.ToString("MM/dd/yyyy HH:mm:ss");
                    DateTime lastDownloadeddate = Convert.ToDateTime(st);



                    int roweffected = _objDustinReportingDbContext.Database.ExecuteSqlCommand("SaveDetailedEventReport @SensorNodeId=@SensorNodeId,@DetectionLog=@DetectionLog,@Datetime=@Datetime,@IsTimeValid=@IsTimeValid,@SensorStatus=@SensorStatus,@ApparelUniqueId=@ApparelUniqueId,@LastDownload=@LastDownload",
                                        new SqlParameter("SensorNodeId", dt.Rows[i][0].ToString().Trim()),
                                        new SqlParameter("DetectionLog", dt.Rows[i][1].ToString().Trim()),
                                        new SqlParameter("Datetime", maindate),
                                        new SqlParameter("IsTimeValid", dt.Rows[i][3].ToString().Trim()),
                                        new SqlParameter("SensorStatus", dt.Rows[i][4].ToString().Trim()),
                                        new SqlParameter("ApparelUniqueId", dt.Rows[i][5].ToString().Trim()),
                                        new SqlParameter("LastDownload", lastDownloadeddate));

                }
                MsgBox.Show("Data Imported Sucessfully...");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                btnUpload.Enabled = false;
            }
        }
        #endregion



      

        private void cbColNameHeader_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
