﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;


namespace DustinDesktopReporting
{
    public class Filesdata
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public string email { get; set; }
    }

    public static class DateTimeUtils
    {
        /// <summary>
        /// <para>Truncates a DateTime to a specified resolution.</para>
        /// <para>A convenient source for resolution is TimeSpan.TicksPerXXXX constants.</para>
        /// </summary>
        /// <param name="date">The DateTime object to truncate</param>
        /// <param name="resolution">e.g. to round to nearest second, TimeSpan.TicksPerSecond</param>
        /// <returns>Truncated DateTime</returns>
        public static DateTime Truncate(this DateTime date, long resolution)
        {
            return new DateTime(date.Ticks - (date.Ticks % resolution), date.Kind);
        }
    }

    public sealed class SyncDataAuto
    {
        private SyncDataAuto()
        {
        }

        private DustinReportingDbContext _objDustinReportingDbContext = new DustinReportingDbContext();
        string _secondColoumn;
        string _thirdColoumn;
        string _lastDownloadDate;
        string SensorNodeId = null;
        string LastDownlodedDateTime = null;
        string SensorStatus = null;

        private volatile static SyncDataAuto singleTonObject;
        private static object lockingObject = new object();

        public static SyncDataAuto InstanceCreation()
        {
            if (singleTonObject == null)
            {
                lock (lockingObject)
                {
                    if (singleTonObject == null)
                    {
                        singleTonObject = new SyncDataAuto();
                    }
                }
            }
            return singleTonObject;
        }

        private static bool InProcess = false;

        public void DoStuff()
         {
            try
            {
                string fileExt = string.Empty;
                if (InProcess == false)
                {
                    InProcess = true;

                    #region Code

                    string email = Properties.Settings.Default.Email;
                    string value1 = _objDustinReportingDbContext.Database.SqlQuery<string>("select Path from UserProfile where EmailId='" + email + "'").FirstOrDefault();

                    if (value1 != null && value1 != "null" && value1 != "")
                    {
                        //var temp = Directory.GetFiles(value1, "*.csv").OrderBy(d => new FileInfo(d).CreationTime);
                        var temp = Directory.GetFiles(value1, "*.*", SearchOption.AllDirectories).Where(s => s.EndsWith(".csv") || s.EndsWith(".xlsx") || s.EndsWith(".xls")).OrderBy(d => new FileInfo(d).CreationTime);

                        string[] files = temp.ToArray();
                        int ab = files.Count();

                        for (int m = 0; m < ab; m++)
                        {
                            try
                            {
                                string filename = System.IO.Path.GetFileName(files[m]);
                                fileExt = Path.GetExtension(filename);//get the file extension
                                int chkFileExist = _objDustinReportingDbContext.Database.SqlQuery<int>("select id from filelog where filename = '" + filename + "'").FirstOrDefault();

                                if (chkFileExist > 0)
                                {
                                    Writer.Log("Skipped - File name " + files[m]);
                                    continue;
                                }
                                else
                                {
                                    Writer.Log("Uploading - File name " + files[m]);
                                }

                                string csvPath1 = files[m];
                                string xlsPath1 = files[m];

                                //Check type of file
                                if (fileExt.CompareTo(".xls") == 0 || fileExt.CompareTo(".xlsx") == 0)
                                {
                                    #region Read .xlsx file

                                    DataTable dt = new DataTable();
                                    try
                                    {
                                        //For making column in datatable
                                        dt.Columns.AddRange(new DataColumn[]
                                                                        {
                                                new DataColumn("SensorNodeId", typeof(string)),
                                                new DataColumn("DetectionLog", typeof(string)),
                                                new DataColumn("Datetime",typeof(string)),
                                                new DataColumn("IsTimeValid",typeof(string)),
                                                new DataColumn("SensorStatus",typeof(string)),
                                                new DataColumn("ApparelUniqueId",typeof(string)),
                                                new DataColumn("LastDownloadDate",typeof(string))
                                                                         });

                                        using (XLWorkbook workBook = new XLWorkbook(xlsPath1))
                                        {
                                            //Read the first Sheet from Excel file.
                                            IXLWorksheet workSheet = workBook.Worksheet(1);

                                            //Create a new DataTable.
                                            //Loop through the Worksheet rows.
                                            bool firstRow = true;
                                            foreach (IXLRow row in workSheet.Rows())
                                            {
                                                //Use the first row to add columns to DataTable.
                                                if (firstRow)
                                                {
                                                    int j = 0;
                                                    foreach (IXLCell cell in row.Cells())
                                                    {
                                                        if (j == 3)
                                                        {
                                                            SensorNodeId = cell.Value.ToString();
                                                            SensorNodeId = SensorNodeId.Replace('-', ':');
                                                        }
                                                        if (j == 1)
                                                        {
                                                            LastDownlodedDateTime = cell.Value.ToString();
                                                        }
                                                        j++;
                                                    }
                                                    firstRow = false;
                                                }
                                                                                              
                                                int val = row.RowNumber();

                                                //Add rows to DataTable.
                                                if (val > 1)
                                                {
                                                    //if detection log is null
                                                    //continue;

                                                    dt.Rows.Add();
                                                    int i = 1;
                                                    foreach (IXLCell cell in row.Cells())
                                                    {
                                                        if (cell.Value.ToString().Contains("System info code"))
                                                        {
                                                            dt.Rows[dt.Rows.Count - 1][i] = 0;
                                                        }
                                                        else if (cell.Value.ToString().Contains("Apparel entered field"))
                                                        {
                                                            dt.Rows[dt.Rows.Count - 1][i] = 1;
                                                        }
                                                        else if (cell.Value.ToString().Contains("Apparel exited field"))
                                                        {
                                                            dt.Rows[dt.Rows.Count - 1][i] = 2;
                                                        }
                                                        else
                                                        {
                                                            dt.Rows[dt.Rows.Count - 1][i] = cell.Value.ToString();
                                                        }

                                                        i++;
                                                        if (i == 7)
                                                        {
                                                            i = 0;
                                                            break;
                                                        }
                                                    }
                                                    dt.Rows[dt.Rows.Count - 1][0] = SensorNodeId;
                                                    dt.Rows[dt.Rows.Count - 1][6] = LastDownlodedDateTime;
                                                }                                               
                                            }                                       

                                        }
                                        //Removing null rows in .xlsx file by checking null values on detection log
                                        int count = dt.Rows.Count;
                                        for (int i = (dt.Rows.Count-1); i >= 0; i--)
                                        {
                                            string val = dt.Rows[i].ItemArray[1].ToString();

                                            if (string.IsNullOrEmpty(val))
                                            {
                                                dt.Rows[i].Delete();
                                            }
                                            else
                                            {
                                                break;
                                            }
                                        }
                                        //loop Removing end
                                    }
                                    catch (Exception ex)
                                    {
                                        MessageBox.Show(ex.Message.ToString());
                                    }

                                    #endregion

                                    #region Inserting data in table from .xlsx file

                                    DataSet da = new DataSet();

                                    for (int i = 0; i <= dt.Rows.Count - 1; i++)
                                    {
                                        DateTime date = DateTime.ParseExact(dt.Rows[i][2].ToString(), "dd/MM/yyyy HH:mm:ss", null);
                                        string s = date.ToString("MM/dd/yyyy HH:mm:ss");
                                        DateTime maindate = Convert.ToDateTime(s);
                                        DateTime lastdate = DateTime.ParseExact(dt.Rows[i][6].ToString(), "dd/MM/yyyy HH:mm:ss", null);
                                        string st = lastdate.ToString("MM/dd/yyyy HH:mm:ss");
                                        DateTime lastDownloadeddate = Convert.ToDateTime(st);

                                        int roweffected = _objDustinReportingDbContext.Database.ExecuteSqlCommand("SaveDetailedEventReport @SensorNodeId=@SensorNodeId,@DetectionLog=@DetectionLog,@Datetime=@Datetime,@IsTimeValid=@IsTimeValid,@SensorStatus=@SensorStatus,@ApparelUniqueId=@ApparelUniqueId,@LastDownload=@LastDownload",
                                                            new SqlParameter("SensorNodeId", dt.Rows[i][0].ToString().Trim()),
                                                            new SqlParameter("DetectionLog", dt.Rows[i][1].ToString().Trim()),
                                                            new SqlParameter("Datetime", maindate),
                                                            new SqlParameter("IsTimeValid", dt.Rows[i][3].ToString().Trim()),
                                                            new SqlParameter("SensorStatus", dt.Rows[i][4].ToString().Trim()),
                                                            new SqlParameter("ApparelUniqueId", dt.Rows[i][5].ToString().Trim()),
                                                            new SqlParameter("LastDownload", lastDownloadeddate));
                                    }

                                    #endregion

                                   int a = _objDustinReportingDbContext.Database.ExecuteSqlCommand("insert into filelog(filename, email, inserteddate) values('" + filename + "', '" + email + "', '" + DateTime.Now + "')");
                                    Writer.Log("Uploaded");
                                }
                                else
                                {
                                    #region csv reading 

                                    DataTable dt1 = new DataTable();
                                    dt1.Columns.AddRange(new DataColumn[]
                                        {
                                                new DataColumn("SensorNodeId", typeof(string)),
                                                new DataColumn("DetectionLog", typeof(string)),
                                                new DataColumn("Datetime",typeof(string)),
                                                new DataColumn("IsTimeValid",typeof(string)),
                                                new DataColumn("SensorStatus",typeof(string)),
                                                new DataColumn("ApparelUniqueId",typeof(string)),
                                                new DataColumn("LastDownloadDate",typeof(string))
                                         });

                                    string csvData1 = File.ReadAllText(csvPath1);//Read the contents of CSV file


                                    //Creating datatable

                                    int k1 = 0;

                                    //Execute a loop over the rows
                                    foreach (string row in csvData1.Split('\n'))
                                    {
                                        if (!string.IsNullOrEmpty(row))
                                        {
                                            if (k1 > 0)
                                            {
                                                dt1.Rows.Add();
                                            }
                                            int i = 1;
                                            k1++;

                                            //Execute a loop over the columns.
                                            if (dt1.Rows.Count > 0)

                                            {
                                                dt1.Rows[dt1.Rows.Count - 1][0] = _thirdColoumn;
                                                dt1.Rows[dt1.Rows.Count - 1][6] = _lastDownloadDate;
                                                foreach (string cell in row.Split(','))
                                                {
                                                    if (i < 6)
                                                    {
                                                        if (cell.Contains("System info code"))
                                                        {
                                                            dt1.Rows[dt1.Rows.Count - 1][i] = 0;
                                                        }
                                                        else if (cell.Contains("Apparel entered field"))
                                                        {
                                                            dt1.Rows[dt1.Rows.Count - 1][i] = 1;
                                                        }
                                                        else if (cell.Contains("Apparel exited field"))
                                                        {
                                                            dt1.Rows[dt1.Rows.Count - 1][i] = 2;
                                                        }
                                                        else
                                                        {
                                                            dt1.Rows[dt1.Rows.Count - 1][i] = cell;
                                                        }
                                                        i++;
                                                    }
                                                }//End foreach
                                            }
                                            else
                                            {
                                                int j = 0;
                                                foreach (string cell in row.Split(','))
                                                {
                                                    switch (j)
                                                    {
                                                        case 1:
                                                            _lastDownloadDate = cell;
                                                            break;
                                                        case 2:

                                                            _secondColoumn = cell;
                                                            break;
                                                        case 3:

                                                            _thirdColoumn = cell.Replace("-", ":");

                                                            break;

                                                        default:
                                                            break;
                                                    }//End switch
                                                    j++;
                                                }//End foreach
                                            }
                                        }
                                    }


                                    #endregion
                                    
                                    #region Inserting data in table

                                    DataSet da = new DataSet();

                                    for (int i = 0; i <= dt1.Rows.Count - 1; i++)
                                    {
                                        DateTime date = DateTime.ParseExact(dt1.Rows[i][2].ToString(), "dd/MM/yyyy HH:mm:ss", null);
                                        string s = date.ToString("MM/dd/yyyy HH:mm:ss");
                                        DateTime maindate = Convert.ToDateTime(s);
                                        DateTime lastdate = DateTime.ParseExact(dt1.Rows[i][6].ToString(), "dd/MM/yyyy HH:mm:ss", null);
                                        string st = lastdate.ToString("MM/dd/yyyy HH:mm:ss");
                                        DateTime lastDownloadeddate = Convert.ToDateTime(st);

                                        int roweffected = _objDustinReportingDbContext.Database.ExecuteSqlCommand("SaveDetailedEventReport @SensorNodeId=@SensorNodeId,@DetectionLog=@DetectionLog,@Datetime=@Datetime,@IsTimeValid=@IsTimeValid,@SensorStatus=@SensorStatus,@ApparelUniqueId=@ApparelUniqueId,@LastDownload=@LastDownload",
                                                            new SqlParameter("SensorNodeId", dt1.Rows[i][0].ToString().Trim()),
                                                            new SqlParameter("DetectionLog", dt1.Rows[i][1].ToString().Trim()),
                                                            new SqlParameter("Datetime", maindate),
                                                            new SqlParameter("IsTimeValid", dt1.Rows[i][3].ToString().Trim()),
                                                            new SqlParameter("SensorStatus", dt1.Rows[i][4].ToString().Trim()),
                                                            new SqlParameter("ApparelUniqueId", dt1.Rows[i][5].ToString().Trim()),
                                                            new SqlParameter("LastDownload", lastDownloadeddate));
                                    }

                                    #endregion

                                    int a = _objDustinReportingDbContext.Database.ExecuteSqlCommand("insert into filelog(filename, email, inserteddate) values('" + filename + "', '" + email + "', '" + DateTime.Now + "')");
                                    Writer.Log("Uploaded");
                                }
                            }
                            catch (Exception ex)
                            {
                                Writer.Log("FileError " + ex.Message);
                                continue;
                            }
                        }//End for loop   
                    }
                    else
                    {
                        Writer.Log("No path found");
                    }

                    #endregion

                    InProcess = false;
                }
                else
                {
                    Writer.Log("Already in process");
                }
            }
            catch (Exception ex)
            {
                Writer.Log("AppError " + ex.Message);
            }
        }
    }
}
