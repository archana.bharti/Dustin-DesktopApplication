﻿using DustinDesktopReporting;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchedulerService
{
    public static class DateTimeUtils
    {
        /// <summary>
        /// <para>Truncates a DateTime to a specified resolution.</para>
        /// <para>A convenient source for resolution is TimeSpan.TicksPerXXXX constants.</para>
        /// </summary>
        /// <param name="date">The DateTime object to truncate</param>
        /// <param name="resolution">e.g. to round to nearest second, TimeSpan.TicksPerSecond</param>
        /// <returns>Truncated DateTime</returns>
        public static DateTime Truncate(this DateTime date, long resolution)
        {
            return new DateTime(date.Ticks - (date.Ticks % resolution), date.Kind);
        }
    }

    public class SyncData
    {
        private DustinReportingDbContext _objDustinReportingDbContext = new DustinReportingDbContext();
        string _secondColoumn;
        string _thirdColoumn;
        string _lastDownloadDate;

        public void DoStuff()
        {
            try
            {            
                string value1 = _objDustinReportingDbContext.Database.SqlQuery<string>("select ReportPath from Path").FirstOrDefault();
                string[] files = Directory.GetFiles(value1, "*.csv");
            
                for (int m = 0; m < files.Count(); m++)
                {
                    DateTime creation = System.IO.File.GetCreationTime(files[m]);
                    var res1 = _objDustinReportingDbContext.Database.SqlQuery<DateTime>("select val from LogFileTime where id = 1").FirstOrDefault();

                    if (Convert.ToDateTime(res1).Truncate(TimeSpan.TicksPerSecond) >= creation.Truncate(TimeSpan.TicksPerSecond))
                        continue;
                        
                    Liberary.WriteLog("File name " + files[m]);

                    string csvPath1 = files[m];

                    #region Creating datatable header

                    DataTable dt1 = new DataTable();
                    dt1.Columns.AddRange(new DataColumn[]
                        {
                        new DataColumn("SensorNodeId", typeof(string)),
                        new DataColumn("DetectionLog", typeof(string)),
                        new DataColumn("Datetime",typeof(string)),
                        new DataColumn("IsTimeValid",typeof(string)),
                        new DataColumn("SensorStatus",typeof(string)),
                        new DataColumn("ApparelUniqueId",typeof(string)),
                        new DataColumn("LastDownloadDate",typeof(string))
                         });

                    string csvData1 = File.ReadAllText(csvPath1);//Read the contents of CSV file

                    #endregion

                    #region Creating datatable

                    int k1 = 0;

                    //Execute a loop over the rows
                    foreach (string row in csvData1.Split('\n'))
                    {
                        if (!string.IsNullOrEmpty(row))
                        {
                            if (k1 > 0)
                            {
                                dt1.Rows.Add();
                            }
                            int i = 1;
                            k1++;
                            //Execute a loop over the columns.
                            if (dt1.Rows.Count > 0)
                            {
                                dt1.Rows[dt1.Rows.Count - 1][0] = _thirdColoumn;
                                dt1.Rows[dt1.Rows.Count - 1][6] = _lastDownloadDate;
                                foreach (string cell in row.Split(','))
                                {
                                    if (i < 6)
                                    {
                                        if (cell.Contains("System info code"))
                                        {
                                            dt1.Rows[dt1.Rows.Count - 1][i] = 0;
                                        }
                                        else if (cell.Contains("Apparel entered field"))
                                        {
                                            dt1.Rows[dt1.Rows.Count - 1][i] = 1;
                                        }
                                        else if (cell.Contains("Apparel exited field"))
                                        {
                                            dt1.Rows[dt1.Rows.Count - 1][i] = 2;
                                        }
                                        else
                                        {
                                            dt1.Rows[dt1.Rows.Count - 1][i] = cell;
                                        }

                                        i++;
                                    }

                                }//End foreach
                            }
                            else
                            {
                                int j = 0;
                                foreach (string cell in row.Split(','))
                                {
                                    switch (j)
                                    {
                                        case 1:
                                            _lastDownloadDate = cell;
                                            break;
                                        case 2:

                                            _secondColoumn = cell;
                                            break;
                                        case 3:

                                            _thirdColoumn = cell.Replace("-", ":");

                                            break;

                                        default:
                                            break;
                                    }//End switch
                                    j++;
                                }//End foreach
                            }
                        }
                    }

                    #endregion

                    #region Inserting data in table

                    DataSet da = new DataSet();

                    for (int i = 0; i <= dt1.Rows.Count - 1; i++)
                    {
                        // DateTime value =Convert.ToDateTime(dt1.Rows[i][2]);
                        DateTime date = DateTime.ParseExact(dt1.Rows[i][2].ToString(), "dd/MM/yyyy HH:mm:ss", null);
                        string s = date.ToString("MM/dd/yyyy HH:mm:ss");
                        DateTime maindate = Convert.ToDateTime(s);
                        // String  datetime= value.ToString("MM/dd/yyyy hh:mm ss tt");

                        DateTime lastdate = DateTime.ParseExact(dt1.Rows[i][6].ToString(), "dd/MM/yyyy HH:mm:ss", null);
                        string st = lastdate.ToString("MM/dd/yyyy HH:mm:ss");
                        DateTime lastDownloadeddate = Convert.ToDateTime(st);

                        int roweffected = _objDustinReportingDbContext.Database.ExecuteSqlCommand("SaveDetailedEventReport @SensorNodeId=@SensorNodeId,@DetectionLog=@DetectionLog,@Datetime=@Datetime,@IsTimeValid=@IsTimeValid,@SensorStatus=@SensorStatus,@ApparelUniqueId=@ApparelUniqueId,@LastDownload=@LastDownload",
                                            new SqlParameter("SensorNodeId", dt1.Rows[i][0].ToString().Trim()),
                                            new SqlParameter("DetectionLog", dt1.Rows[i][1].ToString().Trim()),
                                            new SqlParameter("Datetime", maindate),
                                            new SqlParameter("IsTimeValid", dt1.Rows[i][3].ToString().Trim()),
                                            new SqlParameter("SensorStatus", dt1.Rows[i][4].ToString().Trim()),
                                            new SqlParameter("ApparelUniqueId", dt1.Rows[i][5].ToString().Trim()),
                                            new SqlParameter("LastDownload", lastDownloadeddate));
                    }

                    #endregion
               
                    int res = _objDustinReportingDbContext.Database.ExecuteSqlCommand("update LogFileTime set val = @val", new SqlParameter("val", creation));
                    if (res == 1)
                        Liberary.WriteLog("Date inserted");
                    else
                        Liberary.WriteLog("Date insertion failed");
                    
                }
            }
            catch(Exception ex)
            {
                Liberary.WriteLog("Error " + ex.Message);
            }
        }
    }
}
