﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace SchedulerService
{
    public partial class Service1 : ServiceBase
    {
        SyncData obj;

        public Service1()
        {
            InitializeComponent();
            obj = new SyncData();
        }

        protected override void OnStart(string[] args)
        {
            Liberary.WriteLog("Service started");
            Job();
        }

        public void Job()
        {
            double TimerInterVal = (double)900000; //900000; //After 15 min
            System.Timers.Timer myTimer = new System.Timers.Timer();
            myTimer.Interval = TimerInterVal;
            myTimer.AutoReset = true;
            myTimer.Elapsed += new System.Timers.ElapsedEventHandler(notify_Elapsed);
            myTimer.Enabled = true;
            obj.DoStuff();
        }

        void notify_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Liberary.WriteLog("Elapse");
            obj.DoStuff();
        }

        protected override void OnStop()
        {
            Liberary.WriteLog("Stop");
        }

        public static void Main()
        {
            Service1 ser = new Service1();
            ser.OnStart(null);
            ServiceBase.Run(new Service1());
        }
    }
}
