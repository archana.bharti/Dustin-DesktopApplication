﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DustinDesktopReporting.Data.Models
{
    public class DetailedReportModel
    {
        public string SensorNodeId { get; set; }
        public string DetectionLog { get; set; }
        public string Datetime { get; set; }
        //public Int64 DetectionLog { get; set; }
        //public DateTime Datetime { get; set; }
        public string SensorStatus { get; set; }
        public string ApparelUniqueId { get; set; }
    }
}
