﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DustinDesktopReporting.Data.Models
{
    public class ImportCSV
    {
        private readonly string _CsvFileName;


        public ImportCSV(string aCsvFileName)
        {
            //Save parameters
            _CsvFileName = aCsvFileName;
        }

        private string ConstructErrorMessage(long aLineNumber, string[] aLineContents, string aSpecificError)
        {
            string lineContents = string.Empty;
            foreach (string lineContent in aLineContents)
            {
                if (lineContents != string.Empty)
                {
                    lineContents = "," + lineContents;
                    lineContents += lineContent;
                }
            }

            return string.Format("Line {0}: {1}\n\n{2}", aLineNumber, lineContents, aSpecificError);
        }

        public bool Import(out List<DetailedReportModel> aImportItems, out string aErrorMessage)
        {
            //Default behaviour
            aErrorMessage = string.Empty;
            aImportItems = new List<DetailedReportModel>();

            using (TextReader textReader = File.OpenText(_CsvFileName))
            {
                //textReader.ReadToEnd();

                using (CsvReader csvReader = new CsvReader(textReader))
                {
                    //We have no header record
                    csvReader.Configuration.HasHeaderRecord = false;

                    const int NUMBER_LINES_IGNORE = 1;
                    while (csvReader.Read())
                    {
                        //Skip the first 2 lines as they contain heading data
                        if (csvReader.Row > NUMBER_LINES_IGNORE)
                        {

                            DetailedReportModel dr = new DetailedReportModel();
                            //Field 0: DateTime eg. 04/03/2015 12:15 AM
                            //Field 1: Value eg. 4.0
                            //Field 2: Measurement eg. kWh


                            string columnSensorNodeId;
                            if (!csvReader.TryGetField<string>(1, out columnSensorNodeId))
                            {
                                aErrorMessage = ConstructErrorMessage(csvReader.Row + 1, csvReader.CurrentRecord, "Could not convert the timestamp.");
                                return false;
                            }

                            string DetectionLog;
                            if (!csvReader.TryGetField<string>(2, out DetectionLog))
                            {
                                aErrorMessage = ConstructErrorMessage(csvReader.Row + 1, csvReader.CurrentRecord, "Could not convert the measurement.");
                                return false;
                            }
                           
                            string Datetime;
                            if (!csvReader.TryGetField<string>(3, out Datetime))
                            {
                                aErrorMessage = ConstructErrorMessage(csvReader.Row + 1, csvReader.CurrentRecord, "Could not convert the measurement.");
                                return false;
                            }

                            string columnSensorStatus;
                            if (!csvReader.TryGetField<string>(4, out columnSensorStatus))
                            {
                                aErrorMessage = ConstructErrorMessage(csvReader.Row + 1, csvReader.CurrentRecord, "Could not convert the measurement.");
                                return false;
                            }

                            string columnApparelUniqueId;
                            if (!csvReader.TryGetField<string>(5, out columnApparelUniqueId))
                            {
                                aErrorMessage = ConstructErrorMessage(csvReader.Row + 1, csvReader.CurrentRecord, "Could not convert the measurement.");
                                return false;
                            }

                            dr.SensorNodeId = columnSensorNodeId;
                            dr.DetectionLog = DetectionLog;
                            dr.Datetime = Datetime;
                            dr.SensorStatus = columnSensorStatus;
                            dr.ApparelUniqueId = columnApparelUniqueId;



                            //Got this far so add a new data point item to what we are returning
                            aImportItems.Add(dr);
                        }
                    }
                }
            }

            return true;
        }

    }
}
