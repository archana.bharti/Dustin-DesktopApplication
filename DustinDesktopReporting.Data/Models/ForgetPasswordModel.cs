﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DustinDesktopReporting.Data.Models
{
    public class ForgetPasswordModel
    {
        public Guid EmployeeId { get; set; }
        public string EmailId { get; set; }
        public string Password { get; set; }

    }
}
