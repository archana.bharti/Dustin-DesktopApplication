﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DustinDesktopReporting.Data.Models
{
    public class UserModel
    {
        public Guid? EmployeeId { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string EmailId { get; set; }
        public string Password { get; set; }
      
    }
}
